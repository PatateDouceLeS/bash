#!/bin/bash
#Auteur : MathisPERRIN 14B

mkdir -p OutPuts
touch OutPuts/EviterErreurs
rm OutPuts/*
find -iname "*.png" -o -iname "*.jpg" -o -iname "*.tif" -o -iname "*.gif" -o -iname "*.bmp" > fichtemp2
cut -f 2 -d " " fichtemp2 |rev | cut -f 1 -d "/" |rev > fichtemp
ind=0
while read line
do
        prem=${line:0:3}
        extension=$(echo ${line: -3} |tr A-Z a-z)
        rename="fichier_"$prem"_num_"$ind"."$extension
        ligne_chemin=$(head -$(expr $ind + 1) < fichtemp2 |tail -1)
        cp $ligne_chemin OutPuts/$rename
        ind=$((ind+1))
done < fichtemp
rm fichtemp
rm fichtemp2
