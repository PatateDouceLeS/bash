#!/bin/bash
#Auteur : Mathis PERRIN 14B

mkdir -p OutPuts_Converti
touch OutPuts_Converti/EviterErreurs
rm OutPuts_Converti/*
for img in $(ls OutPuts)
do
        img_png=$(echo ${img::-4}.jpg)
        convert OutPuts/$img -geometry 200x260^ -gravity center -crop 200x260+0+0 OutPuts_Converti/$img_png
done