#!/bin/bash
#Auteur : Mathis PERRIN 14B

du -sh rep_etude
mkdir -p Fich_Img
touch Fich_Img/EviterErreurs
rm -r Fich_Img/*
find -iname "*.png" -o -iname "*.jpg" -o -iname "*.tif" -o -iname "*.gif" -o -iname "*.bmp" > fichtemp
while read line
do
        cp $line Fich_Img
done < fichtemp
rm fichtemp
du -sh Fich_Img
