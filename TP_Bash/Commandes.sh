Comment à taper :
#1. 
du -h .
#2. 
du -sh
#3. 
du |wc -l
#4. 
tree |tail -1
#5. 
tree |grep ".jpg"
#6. 
tree |grep ".jpg\|.png" 
#7. 
find ./rep_etude/ -iname "*jpg"

#8. ./com8.sh (script à lancer) :
mkdir -p OutPuts
touch OutPuts/EviterErreurs
rm OutPuts/*
find -iname "*.jpg" > fichtemp
while read line
do
        mv $line OutPuts
done < fichtemp
rm fichtemp

#9. ./com9.sh (script à lancer) :
cd OutPuts
for img in $(ls)
do
	chmod 600 $img
done 

#10. ./com10.sh (script à lancer) :
du -sh rep_etude
mkdir -p Fich_Img
touch Fich_Img/EviterErreurs
rm -r Fich_Img/*
find -iname "*.png" -o -iname "*.jpg" -o -iname "*.tif" -o -iname "*.gif" -o -iname "*.bmp" > fichtemp
while read line
do
        cp $line Fich_Img
done < fichtemp
rm fichtemp
du -sh Fich_Img

#11. ./com11.sh (script à lancer) :
rm -f empreintes.txt
find rep_etude/ -iname "*.jpg" > fichtemp
while read line
do
        ligne=$(shasum $line)
        echo ${ligne//'  '/':'} >> empreintes.txt
done < fichtemp
rm fichtemp

#12.
cut -f 2 -d ":" empreintes.txt


#13.
cut -f 2 -d ":" empreintes.txt |rev | cut -f 1 -d "/" |rev

#14. ./com14.sh (script à lancer) :
mkdir -p OutPuts
rm OutPuts/*
cut -f 2 -d " " empreintes.txt |rev | cut -f 1 -d "/" |rev > fichtemp
find -iname "*.png" -o -iname "*.jpg" -o -iname "*.tif" -o -iname "*.gif" -o -iname "*.bmp" > fichtemp2
ind=0
while read line
do
        prem=${line:0:3}
        extension=$(echo ${line: -3} |tr A-Z a-z)
        rename="fichier_"$prem"_num_"$ind"."$extension
        ligne_chemin=$(head -$(expr $ind + 1) < fichtemp2 |tail -1)
        cp $ligne_chemin OutPuts/$rename
        ind=$((ind+1))
done < fichtemp
rm fichtemp
rm fichtemp2

#15.
dico=$(cat empreintes.txt)
echo $dico

#16.
nik

#17.

#18. ./com18.sh (script à lancer) :
mkdir -p OutPuts_Converti
touch OutPuts_Converti/EviterErreurs
rm OutPuts_Converti/*
for img in $(ls OutPuts)
do
        img_png=$(echo ${img::-4}.jpg)
        convert $img OutPuts_Converti/$img_png
done

#19. ./com19.sh (script à lancer) :
mkdir -p OutPuts_Converti
touch OutPuts_Converti/EviterErreurs
rm OutPuts_Converti/*
for img in $(ls OutPuts)
do
        img_png=$(echo ${img::-4}.jpg)
        convert OutPuts/$img -geometry 200x260^ -gravity center -crop 200x260+0+0 OutPuts_Converti/$img_png
done

#20.
montage $(find Out -iname "*.jpg") -tile 8x500 -geometry +0+0 mosaique.jpg