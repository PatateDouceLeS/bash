SAE Bash
Pour reproduire mon installation, il faut déjà avoir les bases du bash et il faut faire attention et bien penser à donner les permissions d'execution au ficher bash.
De plus, je conseille d'utiliser VsCode plustôt que la console de commande seulement avec le traitement de texte intégré "nano". Cela fait gagner beaucoup de temps et permet d'éviter des erreurs de part sa simplicité de modification.
Par ailleurs, j'ai fait toute ma SAE sur une machine virtuelle. J'ai fait ce choix car mon PC est assez puissant pour faire tourner les deux OS sans problèmes et le fait de ne pas avoir besoin de redémarrer mon ordinateur pour naviguer entre les OS est fortement pratique.

Quant au programme "install.sh"
J'ai fait le choix d'utiliser des boucle "while" pour donner le choix à l'utilisateur des programme qu'il souhaite installer. 
De plus, j'ai fait seulement des conditionnels if sans fairede elif pour permettre à la commande "All" qui installe tous les programmes de fonctionner plus facilement.
Mon programme est assez simple, donc sa modification pour ajouter l'installation d'un autre programme est facile à faire.

Lien de mon GitLab : https://gitlab.com/PatateDouceLeS/bash.git
