Si vous n'arrivez par à ouvrir le fichier "Rapport.odt", il y a le même contenu ici mais sans mise en forme.

Rapport de SAE Bash


    I. Reformulation de la commande
Ce qui nous était demandé était de créer un ou plusieurs programmes permettant l’installation d’une base depuis une nouvelle machine pour pouvoir travailler sur nos principales matières (python, base de données, développement web…) en installant les paquets ou extensions nécessaires.

    II. Présentation des résultats
J’ai réussi à faire l’installation :
	-de VsCode avec ses extensions utiles, python, autodocstring, pytest…
	-de Java, avec la compilation et le lancement d’un fichier « Hello World » pour vérifier le fonctionnement. 
	Je n’ai pas fait par contre la compilation et l’exécution d’un texte qu’on souhaite.
	-d’un environnement pour le développement Web avec la création d’un dossier qui contient les fichiers pour 
	mettre en place une page : styles.css, positionnement.css et monsite.html (avec la base de la page déjà écrite). 
	J’aurais pu demander le nombre et le nom de fichier voulu.
	-de Git pour pouvoir effectuer des dépôts, du push et pull depuis Git et VsCode.
	-de Python avec l’exécution d’un print(« Hello World »), j’aurais pu faire plus ici mais ça me semblait plus simple 
	de faire l’exécution de VsCode
	-de docker et l’exécution de la commande « docker run hello world » mais je n’ai pas réussi à faire plus avec 
	docker comme l’installation d’Oracle.

	Par ailleurs, j’ai aussi mis en place tout dans un seul fichier avec une interface utilisateurs pour installer 
ce que l’utilisateur souhaite parmi les options disponibles et la possibilité de tout installer avec une option. 
Le programme permet aussi de mettre à jour la liste des paquets et les paquets eux-mêmes ce qui est presque indispensable 
lorsqu’on arrive sur une nouvelle machine.
J’aurais aussi pu pousser plus l’interface utilisateur en mettant du style au texte ou en ajoutant plus d’options ou 
en améliorant la lisibilité par exemple.
    
    III. Explication de la démarche
Au tout début, j’ai fais une boucle while pour l’affichage des options mais ça a été difficile car c’est à ce 
moment que j’ai compris que le bash était très précis. J’ai mis du temps à comprendre que « Espace » et « Alt Gr + Espace » étaient différent…
Mais à l’aide d’internet et du cours Katacoda, j’ai pu réussir et faire un programme répondant à l’utilisateur. 
Ensuite je me suis posé la question de ce qu’il fallait, et dès que j’avais ma liste, je me demandais quoi mettre 
pour chaque élément de ma liste. Par exemple, je me suis dit qu’il fallait VsCode pour éditer du code et dans l’option 
VsCode, il me fallait évidemment l’installation de l’IDE VsCode mais aussi des extensions essentiels plus certaines que j’apprécie.
J’ai aussi rencontré des difficultés pour faire l’installation de Java, je n’avais jamais fait de Java auparavant 
mais j’ai réussi à l’aide de recherches personnels et de l’explication de camarades de classe. Mais sinon j’ai réussi 
globalement à faire tout ce que je souhaitais.
    
    IV. Démonstration de compétence(s)
Durant cette SAE, j'ai utilisé les commandes de base en bash (cd, apt, mkdir, touch, echo...) et aussi les conditionnelles 
et boucles (for et if)que j'ai apprises lors de mes cours, ce qui m'a permis d'installer et configurer un système d’exploitation et 
des outils de développement. De plus, les droits principaux des fichiers créer sont sur l'utilisateur qui à lancer la
commande et il faut le mot de passe de la racine, donc du superutilisateur pour installer ce qui est dans cette SAE, donc la
pérennité des données et des logiciels est assuré. De plus, mon programme est légèrement permissif, donc lorsque vous avez
une réponse à donner au programme, si la réponse ne correspond pas aux choix disponibles, le programme ne va pas dans le mur
et indique que ce n'est pas une bonne réponse. Il y a bien mis en place de mesures correctives adaptées à la nature des incidents identifiés.

    V. Conclusion
De manière général, j’ai pris du plaisir à faire cette SAE sauf pour les erreurs dû à la précision du bash où je me suis un peu arraché les cheveux. Mais sinon j’ai beaucoup appris notamment sur les boucles et les conditionnels en bash, et même le programme m’a été utile lorsque j’ai fait une machine virtuelle sur mon ordinateur principal ce qui m’a permis d’installer facilement et automatiquement ce dont j’avais besoin pour le TP Bash.
Cette SAE m’a d’ailleurs fait encore plus aimer le bash et je regrette un peu de pas avoir fait quelque chose de plus poussé mais je suis quand même fier de ce que j’ai fait et ça me semble quand même assez complet pour mes connaissances actuelles et mes besoins pour l’IUT.